---
title: Using Gitlab Pages and Hugo
image: static/img/posts/using-hugo-gitlab-post-icon.png
date: 2021-07-16
draft: false
series: hugo
tags:
  - hugo
  - gitlab
---

## Your own blog site in four steps

Here's what you will need:

- [x] A gitlab account

yep, that's the whole list! (well, ok... a couple tools on your computer
will make things ___much___ easier, but you don't _need_ them)

and now, here's how you create the site:

- [ ] Start a New Project
- [ ] Choose a ready made template
- [ ] Add content
- [ ] Run the job

### Start a New Project

Just click on the 'New Project' button on the top of your gitlab projects page
![gitlab projects](/static/img/posts/gitlab-projects-view.png)

### Choose a ready made template

Next, choose the 'Create from template' link and choose 'Pages/Hugo' from the
list by clicking 'Use template'
![create from template](/static/img/posts/gitlab-create-new-project.png)
![Hugo template](/static/img/posts/gitlab-hugo-template.png)

After you choose the template, gitlab will bring you back to the project page.
  Name the project `<username>.gitlab.io`

> for example, for this site my project path is
> https://gitlab.com/aldrichtr/aldrichtr.gitlab.io

### Add content

I'll write up another post about getting hugo set up and using some of the basic features, but for now
I'll assume you can get that part started using https://gohugo.io/getting-started

``` sh
hugo new post/my-first-post.md

# run the server so you can see it 'live' as you make changes
hugo server -D
```

the '-D' tells hugo to serve pages marked as 'draft'

This part is important, it's kind of a 'gotcha' until you know about it.
A page is set to `draft: true` by default.  So, although your local server
will show you the page, if you push the repo to gitlab without setting
`draft: false`, you won't see your new page on your site.

I think that is a good thing, that way you can control when a page is visible,
while you work on it and still use all the version control features.

So, once you have the content looking the way you want, the next thing is to
push your changes back to gitlab.

``` sh
git add <content you want pushed>
git commit -m"Setting up my blog with first post"
git push origin <localbranchname>
```

After you merge your changes back into the gitlab repo, you're ready to publish

### Run the job

Depending on how you have configured gitlab ci/cd and what your branching
strategy is, you may need to run the job manually the first time.  You can
check by clicking on "CI/CD -> Pipelines".  If it didn't run, you can click
the 'Run pipeline' button.

Again, probably another post, but I had issues with the included pipeline config

I got a 'git executable not found....' error.  I ended up changing a few things:

I wanted the extended version of hugo so I changed the image to

``` yaml
image: registry.gitlab.com/pages/hugo/hugo_extended:latest
```

To ensure git is in the image before the build:

``` yaml
before_script:
  - apk add --update --no-cache git
```

lastly, I'm not a fan of the word 'master' so i moved it to 'main' and changed
the file accordingly

``` yaml
pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - main
```

## Wrapping up

Now that all of that is in place, when there are changes to the repository, the
pipeline will build the site in the `public` folder inside the `hugo_extended`
container, and push them to your site.

The `pages` job will do all the work for you "behind the scenes".

of course, there are [lots of options](https://docs.gitlab.com/ee/user/project/pages/introduction.html) but hopefully, this is enough to get you started!
