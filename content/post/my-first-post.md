---
title: echo "Hello, World" > blog
description: My first post.  Validating all the components are working.
image: static/img/posts/hello-world-post-icon.png
date: 2021-07-16
draft: false
tags:
  - hello
  - hugo
archives:
  - 2021/07
---

This is my first public post.  I intend to use this space for documenting
and discussing my personal projects.  Over the next few posts, I'll try to lay out
the general subject matter and develop a workflow / schedule for publishing content.

Right now, the general tools are gitlab pages and hugo.  I am currently using Markdown
as the source format, but I've been a long time user of the amazing
[org mode](https://orgmode.org) format, and I see that hugo supports that as well.
